﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Pages.ViewModels;
using Exam12019.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam12019.API
{
    [Route("api/restaurant")]
    public class RestaurantApiController : Controller
    {
        private readonly RestaurantService _RestaurantDbRedis;

        [BindProperty]
        public RestaurantModel FormRestaurant { set; get; }

        public RestaurantApiController (RestaurantService restaurantService)
        {
            this._RestaurantDbRedis = restaurantService;
        }

        [HttpGet(Name = "get-all-data")]
        public async Task<ActionResult<List<RestaurantModel>>> GetAllData()
        {
            var allData = await _RestaurantDbRedis.GetRestaurantAsync();
            if (allData == null)
            {
                return NotFound();
            }
            return allData;
        }

        [HttpGet("{id}", Name = "get-data-by-id")]
        public async Task<ActionResult<RestaurantModel>> GetDataById(Guid id)
        {
            var allData = await _RestaurantDbRedis.GetRestaurantAsync();
            var findData = allData.Where(Q => Q.ID == id).FirstOrDefault();

            if (findData == null)
            {
                return NotFound();
            }

            return findData;
        }

        // POST api/<controller>
        [Route("create")]
        [HttpPost(Name = "create-restaurant")]
        public async Task<ActionResult> Post([FromBody]RestaurantModel restaurantModel)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            await _RestaurantDbRedis.InsertRestaurantAsync(restaurantModel);
            return Ok();
        }

        [Route("edit/{id}")]
        [HttpPost("{id}", Name = "edit-restaurant")]
        public async Task<ActionResult> Update([FromBody]RestaurantModel model, Guid id)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            var allData = await _RestaurantDbRedis.GetRestaurantAsync();
            var findData = allData.FindIndex(Q => Q.ID == id);

            if (findData == -1)
            {
                return NotFound();
            }

            allData[findData] = model;

            await _RestaurantDbRedis.UpdateRestaurantAsync(allData);
            return Ok();
        }

        // DELETE api/<controller>/5
        [Route("delete/{id}")]
        [HttpDelete("{id}", Name = "delete-restaurant")]
        public async Task<ActionResult> Delete(Guid id)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }

            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            var allData = await _RestaurantDbRedis.GetRestaurantAsync();
            var findData = allData.FindIndex(Q => Q.ID == id);

            if (findData == -1)
            {
                return NotFound();
            }

            allData.RemoveAt(findData);

            await _RestaurantDbRedis.UpdateRestaurantAsync(allData);
            return Ok();
        }
    }
}
