﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exam12019.Pages.ViewModels;
using Exam12019.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Exam12019.API
{
    [Route("api/restaurant")]
    public class ReservationApiController : Controller
    {
        private readonly ReservationService _ReservationDbRedis;

        [BindProperty]
        public ReservationModel FormReservation { set; get; }

        public ReservationApiController(ReservationService reservationService)
        {
            this._ReservationDbRedis = reservationService;
        }

        [HttpGet(Name = "get-all-data")]
        public async Task<ActionResult<List<ReservationModel>>> GetAllData()
        {
            var allData = await _ReservationDbRedis.GetReservationAsync();
            if (allData == null)
            {
                return NotFound();
            }
            return allData;
        }

        [HttpGet("{id}", Name = "get-data-by-id")]
        public async Task<ActionResult<ReservationModel>> GetDataById(Guid id)
        {
            var allData = await _ReservationDbRedis.GetReservationAsync();
            var findData = allData.Where(Q => Q.ID == id).FirstOrDefault();

            if (findData == null)
            {
                return NotFound();
            }

            return findData;
        }

        // POST api/<controller>
        [Route("create")]
        [HttpPost(Name = "create-restaurant")]
        public async Task<ActionResult> Post([FromBody]ReservationModel reservationModel)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            await _ReservationDbRedis.InsertReservationAsync(reservationModel);
            return Ok();
        }

    }
}
