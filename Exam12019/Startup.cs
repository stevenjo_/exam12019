using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;
using Microsoft.AspNetCore.DataProtection;
using Exam12019.Services;

namespace Exam12019
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddHttpClient();


            services.AddAuthentication("ClaimIdentityLogin")
                .AddCookie("ClaimIdentityLogin", option => {
                option.AccessDeniedPath = "/Index";
                option.LoginPath = "/Index";
                option.LogoutPath = "/Auth/Logout";
            });

            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = "localhost";
                options.InstanceName = "Exam12019";
            });

            var cm = ConnectionMultiplexer.Connect("localhost");
            services.AddDataProtection().PersistKeysToStackExchangeRedis(cm, "Exam12019LoginEncryptionKey");

            services.AddSession();
            services.AddHttpContextAccessor();
            services.AddScoped<RestaurantService>();
            services.AddScoped<LoginService>();


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
