﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Pages.ViewModels
{
    public class ReservationModel
    {

        public Guid ID { get; set; }

        public Guid RestaurantID { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [StringLength(255)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public DateTime ReservationTime { get; set; }

        
    }
}