﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Exam12019.Pages.ViewModels;
using Exam12019.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace Exam12019.Pages
{
    public class IndexModel : PageModel
    {
        private readonly LoginService _LoginService;

        [BindProperty]
        public LoginModel FormLogin { set; get; } = new LoginModel();
        

        public IndexModel(LoginService loginService)
        {
            this._LoginService = loginService;
        }

        public void OnGet()
        {

        }

        public async Task<IActionResult> OnPostAsync([FromServices] IConfiguration configuration)
        {
            var username = configuration["accelist"];
            var password = configuration["AdminPassword"];
           
            if (ModelState.IsValid == false)
            {
                return Page();
            }
            
            var valid = FormLogin.Username == username && BCrypt.Net.BCrypt.Verify(FormLogin.Password,password);
            
            if (valid == false)
            {
                ModelState.AddModelError("Form.Password", "Invalid login username or passowrd");
                return Page();
            }
            

            var id = new ClaimsIdentity("ClaimIdentityLogin");
            id.AddClaim(new Claim(ClaimTypes.Name, FormLogin.Username));
            id.AddClaim(new Claim(ClaimTypes.NameIdentifier, FormLogin.Username));
            id.AddClaim(new Claim(ClaimTypes.Role, "Administrator"));

            var principal = new ClaimsPrincipal(id);
            await this.HttpContext.SignInAsync("ClaimIdentityLogin", principal, new AuthenticationProperties
            {
                ExpiresUtc = DateTimeOffset.UtcNow.AddYears(1),
                IsPersistent = true
            });

            return RedirectToPage("./Restaurant/Display");
        }
    }
}
