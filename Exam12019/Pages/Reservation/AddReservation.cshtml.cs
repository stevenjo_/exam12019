using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Pages.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.Reservation
{
    public class AddReservationModel : PageModel
    {
        private readonly IHttpClientFactory _HttpFac;

        public AddReservationModel(IHttpClientFactory httpClientFactory)   
        {
            this._HttpFac = httpClientFactory;
        }

        public List<RestaurantModel> restaurantModels { get; set; } = new List<RestaurantModel>();

        [TempData]
        public string SuccessMessage { set; get; }

        [BindProperty]
        public ReservationModel FormReservation { set; get; }
        
        public List<RestaurantModel> Items { set; get; }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var apiUrl = "http://localhost:50508/api/reservation/create";
            var client = _HttpFac.CreateClient();
            var response = await client.PostAsJsonAsync(apiUrl, new ReservationModel
            {
                ID = Guid.NewGuid(),
                Name = FormReservation.Name,
                Email = FormReservation.Email,
                ReservationTime = FormReservation.ReservationTime,
                RestaurantID = FormReservation.RestaurantID

            });

            if (response.IsSuccessStatusCode == false)
            {
                throw new Exception("Insert via API Failed");
            }

            //if ()
            //{
            //    TempData["Error Message"] = "Invalid Restaurant ID";
            //}
            SuccessMessage = "Success Insert";
            TempData["ErrorMessage"] = "Insert Failed";

            return Page();
        }

        public async Task getRestaurant(RestaurantModel restaurantModel)
        {
            var apiUrl = "http://localhost:50508/api/restaurant";
            var client = _HttpFac.CreateClient();
            var respond = await client.GetAsync(apiUrl);

            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("Get All Data via API Failed");
            }

            Items = await respond.Content.ReadAsAsync<List<RestaurantModel>>();
        }

    }
}