﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Pages.ViewModels;
using Exam12019.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.Restaurant
{
    [Authorize(Roles = "Administrator")]
    
    public class CreateModel : PageModel
    {
        private readonly IHttpClientFactory _HttpFac;

        public CreateModel(IHttpClientFactory httpClientFactory)
        {
            this._HttpFac = httpClientFactory;
        }

        [TempData]
        public string SuccessMessage { set; get; }
        [BindProperty]
        public RestaurantModel FormRestaurant { set; get; }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var apiUrl = "http://localhost:50508/api/restaurant/create";
            var client = _HttpFac.CreateClient();
            var response = await client.PostAsJsonAsync(apiUrl, new RestaurantModel
            {
                ID = Guid.NewGuid(),
                Name = FormRestaurant.Name,
                Address = FormRestaurant.Address

            });

            if (response.IsSuccessStatusCode == false)
            {
                throw new Exception("Insert via API Failed");
            }

            SuccessMessage = "Insert Berhasil";
            TempData["ErrorMessage"] = "Insert Gagal";

            return Page();
        }
    }
}