using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Pages.ViewModels;
using Exam12019.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.Restaurant
{
    [Authorize(Roles = "Administrator")]
    public class DisplayModel : PageModel
    {
        
        private readonly IHttpClientFactory _HttpFac;
        public DisplayModel(IHttpClientFactory httpClientFactory)
        {
            this._HttpFac = httpClientFactory;
        }
        [BindProperty]
        public List<RestaurantModel> Items { set; get; }
        public async Task<IActionResult> OnGetAsync()
        {
            var apiUrl = "http://localhost:50508/api/restaurant";
            var client = _HttpFac.CreateClient();
            var respond = await client.GetAsync(apiUrl);

            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("Get All Data via API Failed");
            }

            Items = await respond.Content.ReadAsAsync<List<RestaurantModel>>();
            return Page();
        }
    }
}
