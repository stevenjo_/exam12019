using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Pages.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.Restaurant
{
    [Authorize(Roles = "Administrator")]
    public class EditModel : PageModel
    {
        private readonly IHttpClientFactory _HttpFac;
        public EditModel(IHttpClientFactory httpClientFactory)
        {
            this._HttpFac = httpClientFactory;
        }
        
        [BindProperty]
        public RestaurantModel FormRestaurant { set; get; }
        [BindProperty(SupportsGet = true)]
        public Guid IDFind { set; get; }
        [BindProperty]
        public string Name { get; set; }
        [BindProperty]
        public string Address { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            var apiUrl = "http://localhost:50508/api/restaurant/" + IDFind;
            var client = _HttpFac.CreateClient();
            var respond = await client.GetAsync(apiUrl);

            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("Get by ID via API Failed");
            }

            var content = await respond.Content.ReadAsAsync<RestaurantModel>();
            FormRestaurant = new RestaurantModel
            {
                Name = content.Name,
                Address = content.Address
            };

            Name = content.Name;
            Address = content.Address;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var apiUrl = "http://localhost:50508/api/restaurant/edit/" + IDFind;
            var client = _HttpFac.CreateClient();
            var response = await client.PostAsJsonAsync(apiUrl, new RestaurantModel
            {
                ID = IDFind,
                Name = FormRestaurant.Name,
                Address = FormRestaurant.Address
            });

            if (response.IsSuccessStatusCode == false)
            {
                throw new Exception("Update via API Failed");
            }

            return RedirectToPage("./Display");
        }
    }
}