using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Exam12019.Pages.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Exam12019.Pages.Restaurant
{
    [Authorize(Roles = "Administrator")]
    public class DeleteModel : PageModel
    {
        private readonly IHttpClientFactory _HttpFac;
        public DeleteModel(IHttpClientFactory httpClientFactory)
        {
            this._HttpFac = httpClientFactory;
        }
        [BindProperty(SupportsGet = true)]
        public Guid IDFind { set; get; }

        [BindProperty]
        public string Name { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            var apiUrl = "http://localhost:50508/api/restaurant/" + IDFind;
            var client = _HttpFac.CreateClient();
            var respond = await client.GetAsync(apiUrl);

            if (respond.IsSuccessStatusCode == false)
            {
                throw new Exception("Get by ID via API Failed");
            }

            var content = await respond.Content.ReadAsAsync<RestaurantModel>();
            Name = content.Name;
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var apiUrl = "http://localhost:50508/api/restaurant/delete/" + IDFind;
            var client = _HttpFac.CreateClient();
            var response = await client.DeleteAsync(apiUrl);
            return RedirectToPage("./Display");
        }
    }
}