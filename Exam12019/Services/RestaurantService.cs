﻿using Exam12019.Pages.ViewModels;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Services
{
    public class RestaurantService
    {
        private readonly IDistributedCache Cache;

        public RestaurantService(IDistributedCache distributedCache)
        {
            this.Cache = distributedCache;
        }

        public async Task InsertRestaurantAsync(RestaurantModel restaurant)
        {
            var restaurants = await GetRestaurantAsync();
            if (restaurants == null)
            {
                restaurants = new List<RestaurantModel>();
            }

            restaurants.Add(restaurant);
            var restaurantJson = JsonConvert.SerializeObject(restaurants);
            await Cache.SetStringAsync("Restaurants", restaurantJson);
        }

        public async Task<List<RestaurantModel>> GetRestaurantAsync()
        {
            var valueJson = await Cache.GetStringAsync("Restaurants");
            if (valueJson == null)
            {
                return new List<RestaurantModel>();
            }
            return JsonConvert.DeserializeObject<List<RestaurantModel>>(valueJson);
        }

        public async Task UpdateRestaurantAsync(List<RestaurantModel> restaurants)
        {
            var restaurantJson = JsonConvert.SerializeObject(restaurants);
            await Cache.SetStringAsync("Restaurants", restaurantJson);
        }
    }
}
