﻿using Exam12019.Pages.ViewModels;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Services
{
    public class LoginService
    {
        private readonly IDistributedCache _Cache;

        public LoginService(IDistributedCache distributedCache)
        {
            this._Cache = distributedCache;
        }

        public async Task InsertUserAsync(LoginModel loginModel)
        {
            var allUser = await GetUserAsync();
            if (allUser == null)
            {
                allUser = new List<LoginModel>();
            }

            allUser.Add(loginModel);
            var restaurantJson = JsonConvert.SerializeObject(allUser);
            await _Cache.SetStringAsync("Users", restaurantJson);
        }

        public async Task<List<LoginModel>> GetUserAsync()
        {
            var valueJson = await _Cache.GetStringAsync("Users");
            if (valueJson == null)
            {
                return new List<LoginModel>();
            }
            return JsonConvert.DeserializeObject<List<LoginModel>>(valueJson);
        }

        public List<LoginModel> LoginDBService { set; get; } = new List<LoginModel>();

        public bool AddUser(LoginModel model)
        {
            LoginDBService.Add(model);
            return true;
        }

        public async Task<bool> CheckUserFromRedis(LoginModel model)
        {
            var getUser = await GetUserAsync();
            var loginUser = getUser.Where(Q => Q.Username == model.Username && Q.Password == model.Password);

            if (loginUser.Count() == 0)
            {
                return false;
            }

            return true;
        }

        public bool CheckAuth(LoginModel loginModel)
        {
            var checkUsername = LoginDBService.Where(Q => Q.Username.ToUpper().Equals(loginModel.Username.ToUpper())).FirstOrDefault();
            var checkPassword = LoginDBService.Where(Q => Q.Password.Equals(loginModel.Password)).FirstOrDefault();

            if (loginModel.Username.Equals("a") == false || loginModel.Password.Equals("b") == false)
            {
                return false;
            }
                

            return true;
        }
    }
}
