﻿using Exam12019.Pages.Restaurant;
using Exam12019.Pages.ViewModels;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exam12019.Services
{
    public class ReservationService
    {
        private readonly IDistributedCache Cache;
        private string restaurantJson;

        public ReservationService(IDistributedCache distributedCache)
        {
            this.Cache = distributedCache;
        }

        public async Task InsertReservationAsync(ReservationModel reservation)
        {
            var reservations= await GetReservationAsync();
            if (reservations == null)
            {
                reservations = new List<ReservationModel>();
            }

            reservations.Add(reservation);
            var reservationJson = JsonConvert.SerializeObject(reservations);
            await Cache.SetStringAsync("Reservations", reservationJson);
        }

        public async Task<List<ReservationModel>> GetReservationAsync()
        {
            var valueJson = await Cache.GetStringAsync("Reservations");
            if (valueJson == null)
            {
                return new List<ReservationModel>();
            }
            return JsonConvert.DeserializeObject<List<ReservationModel>>(valueJson);
        }

    }
}
